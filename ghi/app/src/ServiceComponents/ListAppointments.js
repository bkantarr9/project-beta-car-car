import { useEffect, useState } from "react";

function ListAppointments() {
	const [appointments, setAppointments] = useState([]);
	const [automobiles, setAutomobiles] = useState([]);

	const fetchAppointments = async () => {
		const response = await fetch("http://localhost:8080/api/appointments/");

		if (response.ok) {
			const data = await response.json();
			setAppointments(data.appointments);
		}
	};
	useEffect(() => {
		fetchAppointments();
	}, []);

	const fetchAutomobiles = async () => {
		const anotherResponse = await fetch(
			"http://localhost:8100/api/automobiles/",
		);
		if (anotherResponse.ok) {
			const data = await anotherResponse.json();
			setAutomobiles(data.autos);
		}
	};
	useEffect(() => {
		fetchAutomobiles();
	}, []);

	const cancelAppointment = async (id) => {
		const cancelUrl = `http://localhost:8080/api/appointments/${id}/cancel/`;
		const fetchConfig = {
			method: "put",
			headers: {
				"Content-Type": "application/json",
			},
		};
		const response = await fetch(cancelUrl, fetchConfig);
		if (response.ok) {
			const response = await fetch("http://localhost:8080/api/appointments/");
			if (response.ok) {
				const data = await response.json();
				setAppointments(data.appointments);
			}
		}
	};

	const finishAppointment = async (id) => {
		const finishUrl = `http://localhost:8080/api/appointments/${id}/finish/`;
		const fetchConfig = {
			method: "put",
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(finishUrl, fetchConfig);
		if (response.ok) {
			const response = await fetch("http://localhost:8080/api/appointments/");
			if (response.ok) {
				const data = await response.json();
				setAppointments(data.appointments);
			}
		}
	};

	function soldVehicle(vin) {
		for (let auto of automobiles) {
			if (vin === auto["vin"]) {
				return "Yes";
			}
		}
		return "No";
	}

	function formatDate() {
		const date = new Date();
		const timeString = date.toLocaleTimeString(undefined, {
			year: "numeric",
			month: "numeric",
			day: "numeric",
			hour: "numeric",
			minute: "2-digit",
		});
		return timeString.replace(/\s/g, "").replace(",", ", ");
	}

	return (
		<>
			<h1>Service Appointments</h1>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>VIN</th>
						<th>Is VIP?</th>
						<th>Customer name</th>
						<th>Date and Time</th>
						<th>Technician</th>
						<th>Reason</th>
					</tr>
				</thead>
				<tbody>
					{appointments.map((appointment) => {
						console.log(appointments);
						if (appointment.status === "Scheduled") {
							return (
								<tr key={appointment.id}>
									<td>{appointment.vin}</td>
									<td>{soldVehicle(appointment.vin)}</td>
									<td>{appointment.customer}</td>
									<td>{formatDate(appointment.date_time)}</td>
									<td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
									<td>{appointment.reason}</td>
									<td>
										<button
											className="btn btn-danger"
											onClick={() => cancelAppointment(appointment.id)}
										>
											Cancel
										</button>
										<button
											className="btn btn-success"
											onClick={() => finishAppointment(appointment.id)}
										>
											Finished
										</button>
									</td>
								</tr>
							);
						}
					})}
				</tbody>
			</table>
		</>
	);
}

export default ListAppointments;
