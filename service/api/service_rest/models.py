from django.db import models
from django.urls import reverse


# Create your models here.

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)



class Technician(models.Model):
    first_name = models.CharField(max_length=50, null=True, default="Unknown")
    last_name = models.CharField(max_length=50, null=True, default="Unknown")
    employee_id = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def get_api_url(self):
        return reverse("api_detail_technician", kwargs={"id": self.id})



class Appointment(models.Model):
    class Status(models.TextChoices):
        SCHEDULED = "Scheduled", "scheduled"
        CANCELED = "Canceled", "canceled"
        FINISHED = "Finished", "finished"


    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.CharField(max_length=10, choices=Status.choices, default="Scheduled")
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        "Technician",
        related_name="technician",
        on_delete=models.CASCADE,
    )
    def __str__(self):
        return f"{self.customer} {self.vin}"

    def get_api_url(self):
        return reverse("api_appointments", kwargs={"id": self.id})
